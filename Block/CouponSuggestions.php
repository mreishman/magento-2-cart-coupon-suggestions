<?php

namespace Mjr\CouponSuggestions\Block;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\SalesRule\Model\Utility;
use Magento\SalesRule\Model\Rss\Discounts;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class CouponSuggestions extends Template {

    /**
     * @var StoreManagerInterface 
     */
    protected $storeManager;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Utility
     */
    protected $utility;

    /**
     * @var Discounts
     */
    protected $discounts;

    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param CustomerSession $customerSession
     * @param CheckoutSession $checkoutSession
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param Utility $utility
     * @param Discounts $discounts
     * @param array $data
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        ScopeConfigInterface $scopeConfigInterface,
        Utility $utility,
        Discounts $discounts,
        array $data = []
    ) {
        $this->storeManager     = $storeManager;
        $this->customerSession  = $customerSession;
        $this->checkoutSession  = $checkoutSession;
        $this->scopeConfig      = $scopeConfigInterface;
        $this->utility          = $utility;
        $this->discounts        = $discounts;

        parent::__construct($context, $data);
    }

    /**
     * Returns if module enabled / disabled
     *
     * @return bool
     */
    public function checkIfEnabled(): bool
    {
        return $this->scopeConfig->getValue('couponsuggestions/general/enable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * Returns if coupon code is applied to show or hide button
     *
     * @return boolean
     */
    public function hideButton(): bool
    {
        return $this->checkoutSession->getQuote()->getCouponCode() ? true : false;
    }

    /**
     * Returns disclaimer text for popup
     *
     * @return string
     */
    public function getDisclaimerText(): string
    {
        return $this->scopeConfig->getValue('couponsuggestions/popup/disclaimer_text', ScopeInterface::SCOPE_STORE) ?? '';
    }

    /**
     * Returns a list of valid and non valid coupon codes and descriptions
     *
     * @return array
     */
    public function getListOfDiscounts(): array
    {
        $quote = $this->checkoutSession->getQuote();

        $address = $quote->getShippingAddress();
        $items = $quote->getAllVisibleItems();

        //get config values
        $onlyCode = $this->scopeConfig->getValue('couponsuggestions/popup/onlycode', ScopeInterface::SCOPE_STORE);
        $list = explode(',', $this->scopeConfig->getValue('couponsuggestions/popup/coupons', ScopeInterface::SCOPE_STORE));
        $listInvert = $this->scopeConfig->getValue('couponsuggestions/popup/couponselect', ScopeInterface::SCOPE_STORE);

        $discountList = $this->discounts->getDiscountCollection($this->storeManager->getStore()->getId(), $this->customerSession->getCustomer()->getGroupId());
        $qualified = [];
        $nonQualified = [];
        //loop through discounts, group in either valid or not valid
        foreach ($discountList as $discount) {

            //skip if no discount code (determined by setting)
            if ($onlyCode && !$discount->getCode()) {
                continue;
            }

            $discountId = $discount->getRuleId();
            //skip if in list or not in list

            $inArr = in_array($discountId, $list);
            if ($inArr) {
                //discount is in array, skip if inverted
                if($listInvert) {
                    continue;
                }
            } else {
                //discount is not in array, skip if not inverted
                if(!$listInvert) {
                    continue;
                }
            }

            //check if valid
            $validate = $this->utility->canProcessRule($discount, $address);
            $validAction = false;
            foreach ($items as $item) {
                $validAction = $discount->getActions()->validate($item);
                if ($validAction) {
                    break;
                }
            }

            //sort into valid / not valid arrays
            if ($validate && $validAction) {
                $qualified[$discountId] = [
                    'name'          => $discount->getName(),
                    'description'   => $discount->getDescription(),
                    'coupon'        => $discount->getCode()
                ];
            } else {
                $nonQualified[$discountId] = [
                    'name'          => $discount->getName(),
                    'description'   => $discount->getDescription(),
                    'coupon'        => $discount->getCode()
                ];
            }
        }

        return [
            'qualified'     => $qualified,
            'nonqualified'  => $nonQualified
        ];
    }

}