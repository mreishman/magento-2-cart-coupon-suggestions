require([
    "jquery",
    "Magento_Ui/js/modal/modal"
],function($, modal) {

    const options = {
        type: 'popup',
        responsive: true,
        title: 'Coupons',
        buttons: [{
            text: $.mage.__('Close'),
            class: '',
            click: function () {
                this.closeModal();
            }
        }]
    };

    modal(options, $('#coupon-popup-modal'));
    $('#view_coupons_button').click(function() {
        $('#coupon-popup-modal').modal('openModal');
    });
    
    $('#coupon-popup-modal li .coupon-active').click(function() {
            $('#coupon_code').val($(this).data('coupon'));
            $('#discount-coupon-form').submit();
      });
});
