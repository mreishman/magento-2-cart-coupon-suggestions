<?php

namespace Mjr\CouponSuggestions\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class CouponList implements OptionSourceInterface
{
   
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     *  @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory
    ) {
        $this->storeManager         = $storeManager;
        $this->collectionFactory    = $collectionFactory;
    }

    /**
     * @return array
     *
     * Returns all coupons for current site
     */
    public function toOptionArray()
    {
        $options = [];
        $collection = $this->collectionFactory->create();
        $collection->addWebsiteFilter($this->storeManager->getStore()->getId())
            ->addFieldToFilter('is_rss', 1)
            ->setOrder('from_date', 'desc');
        $collection->load();
        foreach ($collection as $discount) {
            $options[] = ['value' => $discount->getRuleId(), 'label' => $discount->getName()];
        }

        return $options;
    }
}
